﻿using System;
using System.Threading;
using System.Windows.Forms;
using TetrisEngine;

namespace TetrisWF
{
    
    public partial class Form1 : Form
    {
        Engine engine;
        Thread t;
        public Form1()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;

            WFFront wFF = new WFFront(Score, TopScore, RestartLabel, GameOverLabel, Board, Hints, this);

            engine = new Engine(23,10, wFF);

            t = new Thread((engine.MainLoop));
            t.Start();
        }
    }
}
