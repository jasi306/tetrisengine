﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using TetrisEngine;
using Label = System.Windows.Forms.Label;

namespace TetrisWF
{
    public class WFFront : Tetris_Front_End
    {
        Label Score;
        Label TopScore;
        Label restartLabel;
        Label gameOverLabel;
        PictureBox Board;
        PictureBox Side;

        static Dictionary<int, SolidBrush> intBrushMap = new Dictionary<int, SolidBrush>()
        {
            { 0,new SolidBrush(Color.Gray)},         //empty
            { 1,new SolidBrush(Color.Blue)},         //Squer
            { 2,new SolidBrush(Color.LightGreen)},   // Long
            { 3,new SolidBrush(Color.Cyan)},         // S shape
            { 4,new SolidBrush(Color.Red)},          // Z shape
            { 5,new SolidBrush(Color.Orange)},       // L shape
            { 6,new SolidBrush(Color.DarkBlue)},     // Revers L Shape
            { 7,new SolidBrush(Color.Yellow)},       // T Shape
            { 255,new SolidBrush(Color.DeepPink)}    //animation
        };
        static Dictionary<SolidBrush, SolidBrush> BrushBrighterBrushMap = new Dictionary<SolidBrush, SolidBrush>();


        const int BlockSize = 10;
        const int BlockSpreding = 2;

        char lastChar = new char();
        int[,] boardArray;
        int[][,] sideArrays;
        public WFFront(Label Score, Label TopScore, Label restartLabel, Label gameOverLabel, PictureBox Board, PictureBox Side, Form f)
        {
            f.KeyPress += (object o, KeyPressEventArgs e) => { lastChar = e.KeyChar; };
            this.restartLabel = restartLabel;
            this.gameOverLabel = gameOverLabel;
            this.Score = Score;
            this.TopScore = TopScore;
            this.Board = Board;
            this.Side = Side;

            foreach (var a in intBrushMap.Values)
            {
                BrushBrighterBrushMap.Add(a, new SolidBrush(Color.FromArgb(((int)((a.Color.R - 255) * 0.7f) + 255), ((int)((a.Color.G - 255) * 0.7f) + 255), ((int)((a.Color.B - 255) * 0.7f) + 255))));
            }

            sideArrays = new int[][,] { new int[0, 0] };
            boardArray = new int[0, 0];

            Board.Paint += Board_Paint;
            Side.Paint += Hints_Paint;
        }


        public char getKeyboardInput()
        {
            char temp = lastChar;
            lastChar = new char();  //Reset when readed
            return temp;
        }

        public void Show(int[,] board, int score, int maxScore, int[][,] nextBricks, bool GameEnded)
        {


            boardArray = board;
            sideArrays = nextBricks;
            try
            {
                if (GameEnded != gameOverLabel.Visible)
                {
                    restartLabel.Invoke(new Action(() => { restartLabel.Visible = GameEnded; }));
                    gameOverLabel.Invoke(new Action(() => { gameOverLabel.Visible = GameEnded; }));
                }
                Board.Invoke(new Action(() => { Board.Refresh(); }));
                Side.Invoke(new Action(() => { Side.Refresh(); }));
                Score.Invoke(new Action(() => { Score.Text = "Score " + score; }));
                Board.Invoke(new Action(() => { TopScore.Text = "TopScore " + maxScore; }));
            }
            catch (System.ComponentModel.InvalidAsynchronousStateException) { } /*jeśli program spróbuję wyknać operację 
                                                                                    po zamknieciu głownego watku.*/
            catch (System.InvalidOperationException) { }  /*jeśli program spróbuję wyknać operację 
                                                            na wątku głównym zanim form się załaduje do końca.*/

        }

        private void Board_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < boardArray.GetLength(0); ++i)
            {
                for (int j = 0; j < boardArray.GetLength(1); ++j)
                {
                    e.Graphics.FillRectangle(BrushBrighterBrushMap[intBrushMap[boardArray[i, j]]], j * (BlockSize + BlockSpreding), i * (BlockSize + BlockSpreding), BlockSize, BlockSize);
                    e.Graphics.FillRectangle(intBrushMap[boardArray[i, j]], j * (BlockSize + BlockSpreding) + 2, i * (BlockSize + BlockSpreding) + 2, BlockSize - 4, BlockSize - 4);
                }
            }
        }
        private void Hints_Paint(object sender, PaintEventArgs e)
        {
            int maxX = 0;
            int sumY = 0;
            for (int i = 0; i < sideArrays.Length; ++i)
            {
                if (maxX < sideArrays[i].GetLength(1)) maxX = sideArrays[i].GetLength(1);
                sumY += sideArrays[i].GetLength(0) + 1;
            }
            sumY--;

            int offSet = 0;
            e.Graphics.FillRectangle(Brushes.Gray, 0, 0, maxX * (BlockSize + BlockSpreding), sumY * (BlockSize + BlockSpreding));
            for (int i = 0; i < sideArrays.Length; ++i)
            {
                for (int j = 0; j < sideArrays[i].GetLength(0); ++j)
                {
                    for (int k = 0; k < sideArrays[i].GetLength(1); ++k)
                    {
                        if (sideArrays[i][j, k] != 0)
                        {
                            e.Graphics.FillRectangle(BrushBrighterBrushMap [intBrushMap[sideArrays[i][j, k]]], k * (BlockSize + BlockSpreding) + BlockSpreding / 2, j * (BlockSize + BlockSpreding) + offSet + BlockSpreding / 2, BlockSize, BlockSize);
                            e.Graphics.FillRectangle(intBrushMap[sideArrays[i][j, k]], k * (BlockSize + BlockSpreding) + BlockSpreding / 2+2, j * (BlockSize + BlockSpreding) + offSet + BlockSpreding / 2+2, BlockSize-4, BlockSize-4);

                        }
                    }
                }
                offSet += (sideArrays[i].GetLength(0) + 1) * (BlockSize + BlockSpreding);
            }
        }
    }
}
