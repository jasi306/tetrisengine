﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TetrisEngine
{
    public interface Tetris_Front_End
    {
        void Show(int[,] board, int points, int max, int[][,] nextBricks, bool gameEnded);
        char getKeyboardInput();
    }
    public class Engine
    {
        bool gameEnded;
        Stats stats;
        Brick b;
        Brick[] nextBricks;
        Board board;
        Tetris_Front_End front;
        System.Diagnostics.Stopwatch watch;
        bool fastUp = false;

        static Random rand;
        List<Brick> allBricksParagons;
        public Engine(int ySize, int xSize, Tetris_Front_End front)
        {
            
            this.front = front;
            IEnumerable<Brick> aux;
            aux = typeof(Brick).Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Brick))).Select(t => (Brick)Activator.CreateInstance(t));

            allBricksParagons = aux.Cast<Brick>().ToList();

            gameEnded = false;

            rand = new Random();
            board = new Board(ySize, xSize);
            b = new Brick(allBricksParagons[rand.Next(0, allBricksParagons.Count())]);
            nextBricks = new Brick[5];

            for (int i = 0; i < nextBricks.Length; ++i)
            {
                nextBricks[i] = new Brick(allBricksParagons[rand.Next(0, allBricksParagons.Count())]);
            }
            int topScore = 0;
            stats = new Stats(topScore);

            watch = new System.Diagnostics.Stopwatch();
            watch.Start();

        }

        public void MainLoop()
        {

            while (true)
            {

                bool changeOnBoard = true;
                if (!gameEnded)
                {
                    switch (front.getKeyboardInput())
                    {
                        case 'q':
                            tryRotate(false);
                            break;
                        case 'e':
                            tryRotate(true);
                            break;
                        case 'a':
                            tryMove(true);
                            break;
                        case 'd':
                            tryMove(false);
                            break;
                        case 's':
                            fastUp = true;
                            changeOnBoard = false;
                            break;
                        default:
                            changeOnBoard = false;
                            break;
                    }
                    if (changeOnBoard) gamePrint(b);
                }
                else
                {
                    if (front.getKeyboardInput() == 'r')
                    {
                        board.DeepClear();
                        stats.Points = 0;
                        gameEnded = false;
                    }
                }
                

                if (watch.ElapsedMilliseconds > 1000 || (fastUp && watch.ElapsedMilliseconds > 100))
                {
                    fastUp = false;
                    watch.Restart();
                    if (!gameEnded)
                    {
                        if (!tryFall())
                        {

                            board.showBrick(b);
                            findAndEraseFullLines();
                            b = nextBricks[0];

                            if (!board.NotColision(b.Shape, b.PosX, b.PosY))  //endgame
                            {
                                board.showBrick(b);
                                //Console.WriteLine("\nGAME OVER");
                                endGameAnimation();
                                //board.DeepClear();
                                //stats.Points = 0;
                            }
                            for (int i = 0; i < 4; ++i)
                            {
                                nextBricks[i] = nextBricks[i + 1];
                            }
                            
                            nextBricks[4] = new Brick(allBricksParagons[rand.Next(0, allBricksParagons.Count())]);

                        }
                    }
                    gamePrint(b);


                }
            }

        }
        private void gamePrint(Brick b)
        {
            board.showBrick(b);
            gamePrint();
            board.hideBrick(b);
        }
        private void gamePrint()
        {
            front.Show(board.Array, stats.Points, stats.Top, nextBricks.Select(e => e.Shape).ToArray(), gameEnded);
        }

        private bool tryFall()
        {
            if (board.NotColision(b.Shape, b.PosX, b.PosY + 1))
            {
                b.PosY++;
                return true;
            }
            return false;
        }
        private bool tryMove(bool left)
        {
            if (board.NotColision(b.Shape, left ? b.PosX - 1 : b.PosX + 1, b.PosY))
            {
                if (left) b.PosX--; else b.PosX++;
                return true;
            }
            return false;

        }
        private bool tryRotate(bool rotateDirection)
        {
            if (board.NotColision(b.Rotate(rotateDirection), b.PosX, b.PosY))
            {
                b.Shape = b.Rotate(rotateDirection);
                return true;
            }
            return false;
        }
        private bool lineIsFull(int i)
        {
            for (int j = 0; j < board.Array.GetLength(1); ++j)
            {
                if (board.Array[i, j] == new char()) return false;

            }
            return true;
        }
        private void findAndEraseFullLines()
        {
            int pointsMultiplikator = 100;
            for (int i = 0; i < board.Array.GetLength(0);)
            {

                if (lineIsFull(i))
                {
                    stats.Points += pointsMultiplikator;
                    pointsMultiplikator += 100;
                    fullLineAnimation(i);
                    board.fallAfterErase(i);
                }
                else i++;
            }
        }

        private void fullLineAnimation(int i)
        {
            int[] array = new int[board.Array.GetLength(1)];
            for (int j = 0; j < array.Length; ++j)
            {
                array[j] = board.Array[i, j];
                board.Array[i, j] = 255;
            }
            System.Threading.Thread.Sleep(200);
            gamePrint();
            for (int j = 0; j < array.Length; ++j)
            {
                board.Array[i, j] = array[j];
            }
            System.Threading.Thread.Sleep(700);
            gamePrint();
            System.Threading.Thread.Sleep(200);
        }
        private void endGameAnimation()
        {
            gameEnded = true;
        }
    }
}
