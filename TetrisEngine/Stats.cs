﻿using System;
using System.IO;

namespace TetrisEngine
{
    class Stats
    {
        static string folderPath = (Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TetrisJana"));
        static string filePath = (Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TetrisJana/Tetris.Tsave"));

        int top;
        int points;
        public int Points
        {
            get => points;
            set
            {
                points = value;
                if (points > top)
                {
                    top = points;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    File.WriteAllText(filePath, top.ToString());
                }
            }
        }
        public int Top{
            get => top;
        }
        public Stats(int top)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            
            if (File.Exists(filePath))
            {
                string lines = File.ReadAllText(filePath);
                this.top = Int32.Parse(lines);
            }
            else
                this.top = top;
            points = 0;
            
        }
        
    }
}
