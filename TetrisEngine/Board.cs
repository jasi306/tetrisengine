﻿namespace TetrisEngine
{
    
    class Board
    {
        public int[,] Array;
        public Board(int ySize, int xSize)
        {
            Array = new int[ySize, xSize];
        }

        public void showBrick(Brick b)
        {
            int x = b.Shape.GetLength(0);
            int y = b.Shape.GetLength(1);
            for (int i = 0; i < x; ++i)
            {
                for (int j = 0; j < y; ++j)
                {
                    if (b.Shape[i, j] != 0)
                    {
                        Array[b.PosY - x / 2 + i, b.PosX - y / 2 + j] = b.Shape[i, j];
                    }
                }
            }
        }

        public void hideBrick(Brick b)
        {
            int x = b.Shape.GetLength(0);
            int y = b.Shape.GetLength(1);
            for (int i = 0; i < x; ++i)
            {
                for (int j = 0; j < y; ++j)
                {
                    if (b.Shape[i, j] != 0)
                    {
                        Array[b.PosY - x / 2 + i, b.PosX - y / 2 + j] = new char();
                    }
                }
            }
        }
        public bool NotColision(int[,] objectArray, int PosX, int PosY)
        {
            int y = objectArray.GetLength(0);
            int x = objectArray.GetLength(1);
            for (int i = 0; i < y; ++i)
            {
                for (int j = 0; j < x; ++j)
                {
                    if (objectArray[i, j] != 0)
                    {
                        if (PosY - y / 2 + i < 0 || PosY - y / 2 + i > Array.GetLength(0)-1) return false;
                        if (PosX - x / 2 + j < 0 || PosX - x / 2 + j > Array.GetLength(1)-1) return false;
                        if (Array[PosY - y / 2 + i, PosX - x / 2 + j] != new char()) return false;
                    }
                }
            }
            return true;
        }
        public void DeepClear()
        {
            int x = Array.GetLength(0);
            int y = Array.GetLength(1);
            for (int i = 0; i < x; ++i)
            {
                for (int j = 0; j < y; ++j)
                {
                    Array[i, j] = new char();
                }
            }
        }
        public void fallAfterErase(int i)
        {
            for (; i > 0; --i)
            {
                for (int j = 0; j < Array.GetLength(1); ++j)
                {
                    Array[i, j] = Array[i - 1, j];
                }
            }
        }
    }
}
