﻿using TetrisEngine;
namespace TetrisConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleFront cF = new ConsoleFront();
            Engine tetris = new Engine(23,10,cF);
            tetris.MainLoop();
        }
    }
}

