﻿using System;
using System.Collections.Generic;
using TetrisEngine;

namespace TetrisConsole
{
    class ConsoleFront : Tetris_Front_End
    {
        static Dictionary<int, char> intCharMap = new Dictionary<int, char>()
        {
            { 0,new char()},
            { 1,'o'},
            { 2,'l'},
            { 3,'S'},
            { 4,'Z'},
            { 5,'L'},
            { 6,'F'},
            { 7,'T'},
            { 255,'X'}
        };
        public char getKeyboardInput()
        {
            char temp=' ';
            while (Console.KeyAvailable) temp =Console.ReadKey().KeyChar;
            return temp;
        }

        public void Show(int[,] board, int score, int maxScore, int[][,] nextBricks,bool GameEnded)
        {

            Console.Clear();
            int currentComponentLenght = 5; //dlugosc statystyk
            int currentComponentCounter = 0;
            int currentComponentID = -1;
            int maxComponentID = 5;


            for (int i = 0; i < board.GetLength(0); ++i)
            {
                //wypisywanie
                Console.Write('H');
                for (int j = 0; j < board.GetLength(1); ++j)
                {
                    Console.Write($"{intCharMap[board[i, j]]}");
                }
                Console.Write('H');
                //------------------
                // dodatki
                if (currentComponentID < maxComponentID)
                {
                    if (currentComponentID == -1)
                    {
                        string text;
                        switch (currentComponentCounter++)
                        {
                            case 0:
                                text = "    TOP:        S - Szybsze spadanie";
                                break;
                            case 1:
                                text = "    " + maxScore;
                                break;
                            case 2:
                                text = "                A D - Przesuwanie w lewo/prawo";
                                break;
                            case 3:
                                text = "    SCORE:      Q E - Obracanie w lewo/prawo";
                                break;
                            case 4:
                                text = "    " + score;
                                break;
                            default:
                                text = "";
                                break;
                        }
                        Console.Write(text);
                    }
                    else
                    {
                        if (currentComponentCounter < 0)
                        {
                            if (currentComponentCounter == -1)
                            {
                                Console.Write($" {currentComponentID + 1}:");
                            }
                        }
                        else
                        {
                            Console.Write("  ");
                            for (int j = 0; j < nextBricks[currentComponentID].GetLength(1); ++j)
                            {
                                Console.Write(intCharMap[nextBricks[currentComponentID][currentComponentCounter, j]]);
                            }
                        }
                        currentComponentCounter++;
                    }
                    if (currentComponentCounter > currentComponentLenght - 1)
                    {
                        currentComponentID++;
                        if ((currentComponentID < maxComponentID))
                        {
                            currentComponentLenght = nextBricks[currentComponentID].GetLength(0);
                            currentComponentCounter = -1; //<---------- 
                            //-2 -> odstep jednego entera miedzy elementami
                            //-1 -> brak odstepu miedzy elementami
                        }
                    }
                }
                Console.Write('\n');
            }
            for (int j = 0; j < board.GetLength(1) + 2; ++j)
            {
                Console.Write('H');
            }
        }
    }
}
